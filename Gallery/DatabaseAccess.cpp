#include "DatabaseAccess.h"
#include "Album.h"
#include <iostream>
#include <io.h>
#include <vector>

std::vector<std::string> vec;   //main vector
std::vector<std::string> vec2;  //temp vector to not mess with the main vec

/// <summary>
/// Constractor
/// </summary>
DatabaseAccess::DatabaseAccess()
{
	this->_db = nullptr;                    //no need to open on constractor, remmber to open in main!
	this->_dbFileName = "galleryDB.sqlite"; //the db name (should be in the same folder with the code)
}

/// <summary>
/// Simple destractor
/// </summary>
DatabaseAccess::~DatabaseAccess()
{
}

/// <summary>
/// Run commands on the db and check errors
/// </summary>
/// <param name="msg">- The message to send (instructions)</param>
/// <returns>The result from sqlite3_exec func</returns>
int DatabaseAccess::sql_with_check(std::string msg) const
{
    char* errMessage;   //where the error message will be stored

    int res = sqlite3_exec(_db, msg.c_str(), callback, nullptr, &errMessage);   //Run command on the db
    if (res != SQLITE_OK)   //if command failed
    {
        std::cout << errMessage << std::endl;   //print the error message
    }
    return res;
}

/// <summary>
/// Get list of all albums
/// </summary>
/// <returns>A list of all albums</returns>
const std::list<Album> DatabaseAccess::getAlbums()
{
    std::string ins = "SELECT USER_ID, NAME, CREATION_DATE FROM ALBUMS";    //instructions command
    std::list<Album> ret;   //the list to return
    int res = sql_with_check(ins);  //the result from the command

    for (int i = 0; i < vec.size(); i += 3) //for every batch of information(3 places in vec) pre user
    {
        ret.push_back(Album(std::stoi(vec[i]), vec[i + 1], vec[i + 2]));    //push the album to the album list
    }
    vec.clear();    //clear the vector to reset the index for next funcs!
    return ret;
}

/// <summary>
/// get the albums of the user
/// </summary>
/// <param name="user">- The owner of the albums</param>
/// <returns>A list of the albums</returns>
const std::list<Album> DatabaseAccess::getAlbumsOfUser(const User& user)
{
    std::string ins = "SELECT USER_ID, NAME, CREATION_DATE FROM ALBUMS WHERE USER_ID = " + std::to_string(user.getId()) + ";";  //instructions command
    std::list<Album> ret;   //the list to return
    int res = sql_with_check(ins);  //the result from the command

    for (int i = 0; i < vec.size(); i += 3) //for every batch of information(3 places in vec) per user
    {
        ret.push_back(Album(std::stoi(vec[i]), vec[i + 1], vec[i + 2]));    //push the album to the album list
    }
    vec.clear();    //clear the vector to reset the index for next funcs!
    return ret;
}

/// <summary>
/// Create an album
/// </summary>
/// <param name="album">- The album to create in the db</param>
void DatabaseAccess::createAlbum(const Album& album)
{
    int res;    //the result from the command
    std::string ins = "INSERT INTO ALBUMS (NAME, CREATION_DATE, USER_ID) VALUES ("; //instructions command
    ins += "\"" + album.getName() + "\", ";
    ins += "\"" + album.getCreationDate() + "\", ";
    ins += "" + std::to_string(album.getOwnerId()) + ");";

    res = sql_with_check(ins);  //run the command
    vec.clear();    //clear the vector to reset the index for next funcs!
}

/// <summary>
/// Delete an album
/// </summary>
/// <param name="albumName">- Name of the album to delete</param>
/// <param name="userId">- The user id of the album owner</param>
void DatabaseAccess::deleteAlbum(const std::string& albumName, int userId)
{
    int res;    //the result from the command
    std::string ins;    //instructions command
    std::string albId;  //the id of the album

    if (doesAlbumExists(albumName, userId)) //if album exists
    {
        //Find album id
        ins = "SELECT ID FROM ALBUMS WHERE NAME LIKE(\"" + albumName + "\") AND USER_ID = " + std::to_string(userId) + ";"; //get the id of the album to delete
        res = sql_with_check(ins);  //run the command
        albId = vec[0]; //extrac needed information from vector
        vec.clear();    //clear the vector to reset the index for next actions!

        //Get all pictures in the album
        ins = "SELECT ID FROM PICTURES WHERE ALBUM_ID = " + albId + ";";    //get pictures id
        res = sql_with_check(ins);  //run the command

        //Untag the picture and delete it
        for (int i = 0; i < vec.size(); i++)    //for every picture id in vec
        {
            ins = "DELETE FROM TAGS WHERE USER_ID = " + std::to_string(userId) + " AND PICTURE_ID = " + vec[i] + ";";   //delete picture tags
            res = sql_with_check(ins);  //run the command
            ins = "DELETE FROM PICTURES WHERE ALBUM_ID = " + albId + ";";   //delete the picture itself
            res = sql_with_check(ins);  //run the command
        }

        //Delete the album
        ins = "DELETE FROM ALBUMS WHERE ID = " + albId + ";";   //delete the album
        ins += std::to_string(userId);
        ins += ";";
        res = sql_with_check(ins);  //run the command
    }
    else    //if there is no album with such name
    {
        std::cout << "There is no album with the name: " + albumName + " for user with id - " + std::to_string(userId); //inform the user
    }
    vec.clear();    //clear the vector to reset the index for next funcs!
}

/// <summary>
/// Check if album exists
/// </summary>
/// <param name="albumName">- The name of the album to check</param>
/// <param name="userId">- under which user the album should be</param>
/// <returns>true if exists</returns>
bool DatabaseAccess::doesAlbumExists(const std::string& albumName, int userId)
{
    int res;    //the result from the command
    std::string ins = "SELECT ID FROM ALBUMS WHERE NAME = \"" + albumName + "\" AND USER_ID = " + std::to_string(userId) + ";"; //instructions command

    res = sql_with_check(ins);  //run the command
    if (vec.size() > 0) //if there is id in the vec
    {
        vec.clear();    //clear the vector to reset the index for next funcs!
        return true;
    }
    vec.clear();    //clear the vector to reset the index for next funcs!
    return false;
}

/// <summary>
/// Loads the album and all pictures of album
/// </summary>
/// <param name="albumName">- the album to load the pictures from</param>
/// <returns>The album with all the pictures in it</returns>
Album DatabaseAccess::openAlbum(const std::string& albumName)
{
    int res;    //the result from the command
    std::string albId;  //the id of the album to load
    Album *alb = nullptr;   //the album to return
    Picture *pic;   //dynamic picture object to load the pictures of the album
    std::string ins = "SELECT ID FROM ALBUMS WHERE NAME = \"" + albumName + "\"";   //get album id

    res = sql_with_check(ins);  //run the command
    albId = vec[0]; //extract the needed info from vec
    vec.clear();    //clear the vector to reset the index for next actions!

    ins = "SELECT USER_ID, NAME, CREATION_DATE FROM ALBUMS WHERE ID = " + albId + ";";  //get album info
    res = sql_with_check(ins);

    alb = new Album(std::stoi(vec[0]), vec[1], vec[2]); //init the album
    vec.clear();    //clear the vector to reset the index for next actions!

    ins = "SELECT ID, NAME, LOCATION, CREATION_DATE FROM PICTURES WHERE ALBUM_ID = " + albId + ";"; //get info of pictures in album
    res = sql_with_check(ins);

    for (int i = 0; i < vec.size(); i += 4) //for every batch of information(4 places in vec) per user
    {
        pic = new Picture(std::stoi(vec[i]), vec[i + 1], vec[i + 2], vec[i + 3]);   //create new picture with picture iformation

        ins = "SELECT USER_ID FROM TAGS WHERE PICTURE_ID = " + vec[i] + ";";    //get all users that are tagged in the picture
        char* errMessage;   //where the error message will be saved
        res = sqlite3_exec(_db, ins.c_str(), callback2, nullptr, &errMessage);  //execute command and save in secondery vector
        if (res != SQLITE_OK)   //if not succeeded
        {
            std::cout << errMessage << std::endl;   //print the error
        }
        for (int j = 0; j < vec2.size(); j++)   //for every id in the secondery vec
        {
            pic->tagUser(std::stoi(vec2[j]));   //add tag to the pic
        }
        vec2.clear();   //clear the vector
        alb->addPicture(*pic);  //add the picture to the album
    }

    vec.clear();    //clear primary vector
    return *alb;
}

/// <summary>
/// Closes the album
/// </summary>
/// <param name="pAlbum">- The album to close</param>
void DatabaseAccess::closeAlbum(Album& pAlbum)
{
    //no need to close the album, the memmory will free itself
}

/// <summary>
/// Print all albums
/// </summary>
void DatabaseAccess::printAlbums()
{
    int res;    //the result of the command
    std::string ins = "SELECT ALBUMS.NAME, USERS.NAME, ALBUMS.CREATION_DATE FROM ALBUMS JOIN USERS ON ALBUMS.USER_ID = USERS.ID;";  //get info about the albums and users
    res = sql_with_check(ins);  //run the command

    std::cout << "Album list:" << std::endl;
    std::cout << "-----------" << std::endl;
    for (int i = 0; i < vec.size(); i += 3) //print all the albums
    {
        std::cout << "   * [" + vec[i] + "] - created by " + vec[i + 1] + " (created in: " + vec[i + 2] + ")" << std::endl;
    }
    vec.clear();    //clear the vec
}

/// <summary>
/// Adding picture to album by name
/// </summary>
/// <param name="albumName">- The name of the album to add</param>
/// <param name="picture">- The name of the picture to add</param>
void DatabaseAccess::addPictureToAlbumByName(const std::string& albumName, const Picture& picture)
{
    int res;    //the result of the command
    std::string albId;  //the album id
    std::string ins = "SELECT ID FROM ALBUMS WHERE NAME = \"" + albumName + "\"";   //the command instruction - get album id

    res = sql_with_check(ins);  //run command
    albId = vec[0]; //extract needed info
    vec.clear();

    ins = "INSERT INTO PICTURES (NAME, LOCATION, CREATION_DATE, ALBUM_ID) VALUES (";    //insert the picture to the db
    ins += "\"" + picture.getName() + "\", ";
    ins += "\"" + picture.getPath() + "\", ";
    ins += "\"" + picture.getCreationDate() + "\", ";
    ins += albId + ");";
    res = sql_with_check(ins);
    vec.clear();    //clear vec
}

/// <summary>
/// Remove picture from album by name
/// </summary>
/// <param name="albumName">- The name of the album</param>
/// <param name="pictureName">- The name of the picture</param>
void DatabaseAccess::removePictureFromAlbumByName(const std::string& albumName, const std::string& pictureName)
{
    int res;    //the result of the command
    std::string usrId;  //user id
    std::string ins = "SELECT USER_ID FROM ALBUMS WHERE NAME = \"" + albumName + "\" AND (SELECT ALBUM_ID FROM PICTURES WHERE NAME = \"" + pictureName + "\");";    //command instruction - get user id
    
    res = sql_with_check(ins);  //run the command
    if (vec.size() != 1)    //if there is more than or less than one album
    {
        std::cout << "There is no album with this picture or there is more than one album with this picture" << std::endl;
    }
    else
    {
        usrId = vec[0]; //get user if
        vec.clear();    //clear vec
        untagUserInPicture(albumName, pictureName, std::stoi(usrId));   //untag the pic
        ins = "DELETE FROM PICTURES WHERE ID = (SELECT ID FROM PICTURES WHERE NAME = \"" + pictureName + "\" AND ALBUM_ID = (SELECT ID FROM ALBUMS WHERE NAME = \"" + albumName + "\"));";  //delete the pic
        res = sql_with_check(ins);  //run command
    }
    vec.clear();
}

/// <summary>
/// Tag user in picture
/// </summary>
/// <param name="albumName">- The namr of the album</param>
/// <param name="pictureName">- The name of the picture</param>
/// <param name="userId">- The id of the user</param>
void DatabaseAccess::tagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId)
{
    int res;    //the result of the command
    std::string albId;  //album if
    std::string ins = "SELECT ID FROM ALBUMS WHERE NAME = \"" + albumName + "\";";  //command instruction - get album id

    res = sql_with_check(ins);  //run command
    albId = vec[0]; //extract info from vec
    vec.clear();    //clear vec

    ins = "INSERT INTO TAGS (PICTURE_ID, USER_ID) VALUES (";    //insrt tag to the pic
    ins += "(SELECT ID FROM PICTURES WHERE NAME = \"" + pictureName + "\" AND ALBUM_ID = " + albId + "), ";
    ins += std::to_string(userId) + ");";
    res = sql_with_check(ins);  //run command
    vec.clear();    //clear vec
}

/// <summary>
/// Untag user in picture
/// </summary>
/// <param name="albumName">- Album name</param>
/// <param name="pictureName">- Pivture name</param>
/// <param name="userId">- User id</param>
void DatabaseAccess::untagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId)
{
    int res;    //command result
    std::string albId;  //album id
    std::string ins = "SELECT ID FROM ALBUMS WHERE NAME = \"" + albumName + "\";";  //command instruction - get album id

    res = sql_with_check(ins);  //run the command
    albId = vec[0]; //extract info from vec
    vec.clear();    //clear vec

    ins = "DELETE FROM TAGS WHERE PICTURE_ID = ";   //delete tag from piv
    ins += "(SELECT ID FROM PICTURES WHERE NAME = \"" + pictureName + "\" AND ALBUM_ID = " + albId + ") AND ";
    ins += "USER_ID = " + std::to_string(userId) + ";";
    res = sql_with_check(ins);  //run command
    vec.clear();    //clear vec
}

/// <summary>
/// Print users
/// </summary>
void DatabaseAccess::printUsers()
{
    int res;    //result of command
    std::string ins = "SELECT * FROM USERS";    //command instruction - get all users
    
    res = sql_with_check(ins);  //run command
    for (int i = 0; i < vec.size(); i += 2) //print all users
    {
        std::cout << "   + @" + vec[i] + " - " + vec[i + 1] << std::endl;
    }
    vec.clear();    //clear vec
}

/// <summary>
/// Get user by id
/// </summary>
/// <param name="userId">- The id of the user</param>
/// <returns>User object of the user</returns>
User DatabaseAccess::getUser(int userId)
{
    int res;    //command result
    User* usr = nullptr;    //the user to return
    std::string ins = "SELECT * FROM USERS WHERE ID = " + std::to_string(userId) + ";"; //command instruction - get info of user with given id

    if (doesUserExists(userId)) //if user with id exists
    {
        res = sql_with_check(ins);  //run the command
        usr = new User(std::stoi(vec[0]), vec[1]);  //extract info from vec
    }
    else
    {
        std::cout << "No user with if of " + std::to_string(userId) << std::endl;   //inform user
    }
    vec.clear();    //clear vec
    return *usr;
}

/// <summary>
/// Create user
/// </summary>
/// <param name="user">- User object to create from</param>
void DatabaseAccess::createUser(User& user)
{
    int res;    //command info
    std::string ins = "INSERT INTO USERS (NAME) VALUES (";  //command instruction

    ins += "\"" + user.getName() + "\");";
    res = sql_with_check(ins);  //run command
    
    //(no need to push to user object)
    vec.clear();    //clean vec
}

/// <summary>
/// Delete user
/// </summary>
/// <param name="user">- User object to delete from db</param>
void DatabaseAccess::deleteUser(const User& user)
{
    int res;    //command result
    std::string ins = "SELECT NAME FROM ALBUMS WHERE USER_ID = " + std::to_string(user.getId()) + ";";  //command info - get name of albums

    res = sql_with_check(ins);  //run
    for (int i = 0; i < vec.size(); i++)    //delete every album in vec
    {
        deleteAlbum(vec[i], user.getId());
    }
    vec.clear();    //clear vec

    ins = "DELETE FROM USERS WHERE ID = " + std::to_string(user.getId()) + ";"; //delete the user itself
    sql_with_check(ins);    //run
    vec.clear();    //clear vec
}

/// <summary>
/// Check if user exists
/// </summary>
/// <param name="userId">- The user id</param>
/// <returns>true if exists</returns>
bool DatabaseAccess::doesUserExists(int userId)
{
    int res;    //command result
    std::string ins = "SELECT * FROM USERS WHERE ID = " + std::to_string(userId) + ";"; //command instruction

    res = sql_with_check(ins);  //run
    if (!vec.empty())   //if the vec isn't empty
    {
        vec.clear();    //clear vec
        return true;
    }
    vec.clear();    //clear vec
    return false;
}

/// <summary>
/// Count how many albums the user own
/// </summary>
/// <param name="user">- The name of the user</param>
/// <returns>How many albums the user own</returns>
int DatabaseAccess::countAlbumsOwnedOfUser(const User& user)
{
    int res;    //command result
    int ret = 0;    //return value
    std::string ins = "SELECT COUNT(*) FROM ALBUMS WHERE USER_ID = " + std::to_string(user.getId()) + ";";  //command info - num of albums owned

    if (doesUserExists(user.getId()))   //if user exists
    {
        res = sql_with_check(ins);  //run
        ret = std::stoi(vec[0]);    //set return to vec
    }
    vec.clear();    //clear vec
    return ret;
}

/// <summary>
/// Count how many albums tagged of user
/// </summary>
/// <param name="user">- User to look for</param>
/// <returns>How many albums tagged of user</returns>
int DatabaseAccess::countAlbumsTaggedOfUser(const User& user)
{
    int res;    //command result
    int tags = 0;   //return value
    std::vector<std::string> albId; //album ids
    std::vector<std::string> picId; //pic ids
    std::string ins = "SELECT ID FROM ALBUMS WHERE USER_ID = " + std::to_string(user.getId()) + ";";    //command instruction - get users' albums

    res = sql_with_check(ins);  //run command
    for (int i = 0; i < vec.size(); i++)    //push all album ids to the vector
    {
        albId.push_back(vec[i]);
    }
    vec.clear();    //clear vec

    for (int i = 0; i < albId.size(); i++)  //get all pictures in the albums
    {
        ins = "SELECT ID FROM PICTURES WHERE ALBUM_ID = " + albId[i] + ";"; //get pics
        res = sql_with_check(ins);  //run
    }

    for (int i = 0; i < vec.size(); i++)    //push all pi ids to the vector
    {
        picId.push_back(vec[i]);
    }
    vec.clear();    //clear vec

    for (int i = 0; i < picId.size(); i++)  //get all tag ids for all pic ids
    {
        ins = "SELECT USER_ID FROM TAGS WHERE PICTURE_ID = " + picId[i] + ";";   //get tag ids
        res = sql_with_check(ins);  //run
    }
    tags = vec.size();  //set vec size as return val
    vec.clear();    //clear vec
    return tags;
}

/// <summary>
/// Count tags of user
/// </summary>
/// <param name="user">- User to look for</param>
/// <returns>Num of tags of user</returns>
int DatabaseAccess::countTagsOfUser(const User& user)
{
    int res;    //the command result
    int tags = 0;   //return value
    std::string ins;    //command info

    ins = "SELECT COUNT(*) FROM TAGS WHERE PICTURE_ID = " + std::to_string(user.getId()) + ";"; //count all tags of user
    res = sql_with_check(ins);  //run
    tags = std::stoi(vec[0]);   //get info from vec
    vec.clear();    //clear vec
    return tags;
}

/// <summary>
/// Average tags per album of user
/// </summary>
/// <param name="user">- The user to look for</param>
/// <returns>Average tags per album of user</returns>
float DatabaseAccess::averageTagsPerAlbumOfUser(const User& user)
{
    int res;    //command result
    float avg;  //return value
    std::vector<std::string> albId; //album ids
    std::vector<std::string> picId; //pic ids
    std::string ins = "SELECT ID FROM ALBUMS WHERE USER_ID = " + std::to_string(user.getId()) + ";";    //get users' album ids

    res = sql_with_check(ins);  //run
    for (int i = 0; i < vec.size(); i++)    //push all album ids to the vector
    {
        albId.push_back(vec[i]);
    }
    vec.clear();    //clear vec
    if (albId.size() == 0)  //cant divide by 0
    {
        return 0;
    }
    avg = countAlbumsTaggedOfUser(user);    //set num of tagged user
    avg /= albId.size();    //divide by number of albums
    vec.clear();    //clear vec again for safety
	return avg;
}

/// <summary>
/// Get to tagged user
/// </summary>
/// <returns>The top tagged user</returns>
User DatabaseAccess::getTopTaggedUser()
{
    int res;    //the command result
    std::string ins;    //command information
    int count = 0;  //top tags count
    int curr;   //current current count
    int topIndex;   //top index
    std::vector<std::string> users; //vector of all users
    User usr = User(-1, "No Pictures Yet!");    //if there are no pictures

    ins = "SELECT ID FROM USERS;";  //get all users id
    res = sql_with_check(ins);  //run
    for (int i = 0; i < vec.size(); i++)    //push all users to the vector
    {
        users.push_back(vec[i]);
    }
    vec.clear();    //clear vec

    for (int i = 0; i < users.size(); i++)  //get count of tags per user
    {
        ins = "SELECT COUNT(*) FROM TAGS WHERE USER_ID = " + users[i] + ";";
        res = sql_with_check(ins);
    }

    for (int i = 0; i < vec.size(); i++)    //check which user have the biggest number of tags
    {
        curr = std::stoi(vec[i]);
        if (curr > count)
        {
            count = curr;   //set top count
            topIndex = i;   //set top index
        }
    }
    if (!vec.empty())   //if the vec not empty
    {
        usr = getUser(std::stoi(users[topIndex]));  //set user
    }
    vec.clear();    //clear vec
    return usr;
}

/// <summary>
/// Get top tagger picture
/// </summary>
/// <returns>Pivture object of the most tagged pic</returns>
Picture DatabaseAccess::getTopTaggedPicture()
{
    int res;    //command result
    std::string ins;    //command info
    int count = 0;  //top tagged
    int curr;   //current taggs count
    int topIndex;   //top index
    std::vector<std::string> pics;  //vector for all the pic ids
    Picture pic = Picture(-1, "No Pictures Yet!");  //if pic does not exists

    ins = "SELECT ID FROM PICTURES;";   //get all pic ids
    res = sql_with_check(ins);  //run
    for (int i = 0; i < vec.size(); i++)    //push all pic ids to the vec
    {
        pics.push_back(vec[i]);
    }
    vec.clear();    //clear vec

    for (int i = 0; i < pics.size(); i++)   //count tags per pic
    {
        ins = "SELECT COUNT(*) FROM TAGS WHERE PICTURE_ID = " + pics[i] + ";";
        res = sql_with_check(ins);
    }

    for (int i = 0; i < vec.size(); i++)    //find the most tagged pic
    {
        curr = std::stoi(vec[i]);
        if (curr > count)
        {
            count = curr;   //set top count
            topIndex = i;   //set top index
        }
    }
    if (!vec.empty())   //if the vectop not empty
    {
        pic = getPicture(pics[topIndex]);   //set pic object
    }
    vec.clear();    //clear vec
    return pic;
}

/// <summary>
/// Get tagged pictures of user
/// </summary>
/// <param name="user">- The user to look for</param>
/// <returns>The tagged pictures</returns>
std::list<Picture> DatabaseAccess::getTaggedPicturesOfUser(const User& user)
{
    int res;    //command result
    std::string ins;    //command instruction
    std::list<Picture> pics;    //return list
    int i, len; //len and for loop

    ins = "SELECT DISTINCT PICTURE_ID FROM TAGS WHERE USER_ID = " + std::to_string(user.getId()) + ";"; //select all pic ids
    res = sql_with_check(ins);  //run

    len = vec.size();   //get vec size
    for (i = 0; i < len; i++)   //push all pics
    {
        pics.push_back(getPicture(vec[i]));
    }
    vec.clear();    //clear vec

    return pics;
}

bool DatabaseAccess::open()
{
    int doesFileExist = _access(_dbFileName.c_str(), 0);
    int res = sqlite3_open(_dbFileName.c_str(), &_db);
    if (res != SQLITE_OK) {
        std::cout << "Failed to open DB" << std::endl;
        return false;
    }
    if (doesFileExist == -1) {
        const char* sqlStatement[] = {
        "CREATE TABLE USERS (\
        ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,\
        NAME TEXT NOT NULL);",
        "CREATE TABLE ALBUMS (\
        ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,\
        NAME TEXT NOT NULL,\
        CREATION_DATE DATETIME NOT NULL,\
        USER_ID INTEGER,\
        FOREIGN KEY (USER_ID) REFERENCES USERS(ID));",
        "CREATE TABLE PICTURES (\
        ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,\
        NAME TEXT NOT NULL,\
        LOCATION TEXT NOT NULL,\
        CREATION_DATE DATETIME NOT NULL,\
        ALBUM_ID INTEGER NOT NULL,\
        FOREIGN KEY (ALBUM_ID) REFERENCES ALBUMS(ID));",
        "CREATE TABLE TAGS (\
        ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,\
        PICTURE_ID INTEGER NOT NULL,\
        USER_ID INTEGER NOT NULL,\
        FOREIGN KEY (PICTURE_ID) REFERENCES PICTURES(ID),\
        FOREIGN KEY (USER_ID) REFERENCES USERS(ID));" };

        for (int i = 0; i < 4; i++)
        {
            char* errMessage = nullptr;
            res = sqlite3_exec(_db, sqlStatement[i], nullptr, nullptr, &errMessage);
            if (res != SQLITE_OK)
            {
                std::cout << errMessage << std::endl;
                return false;
            }
        }
    }
    return true;
}

void DatabaseAccess::close()
{
    sqlite3_close(_db);
}

void DatabaseAccess::clear()
{
    _db = nullptr;
}

Picture DatabaseAccess::getPicture(std::string picId)
{
    int res;
    Picture* pic = nullptr;
    std::string ins = "SELECT * FROM PICTURES WHERE ID = " + picId + ";";

    //cant use send func couse i need to push to another vector
    char* errMessage;
    res = sqlite3_exec(_db, ins.c_str(), callback2, nullptr, &errMessage);
    if (res != SQLITE_OK)
    {
        std::cout << errMessage << std::endl;
    }

    if (!vec2.empty())
    {
        pic = new Picture(std::stoi(vec2[0]), vec2[1], vec2[2], vec2[3]);
    }
    vec2.clear();
    return *pic;
}

int DatabaseAccess::callback(void* data, int argc, char** argv, char** col)
{
    for (int i = 0; i < argc; i++)
    {
        vec.push_back(argv[i]);
    }
    return 0;
}

int DatabaseAccess::callback2(void* data, int argc, char** argv, char** col)
{
    for (int i = 0; i < argc; i++)
    {
        vec2.push_back(argv[i]);
    }
    return 0;
}
